"use strict";

var express = require("express");
var bodyParser = require("body-parser");
var Sequelize = require ("sequelize");


var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const NODE_PORT = process.env.PORT || 4000;
const EMPLOYEE_API = "/api/employees"; //reuse below

//console.log(Sequelize);
const SQL_USERNAME="root";
const SQL_PASSWORD="fsf123"
var connection = new Sequelize(
    'employees',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3306,
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

var Employees = require('./models/employees')(connection, Sequelize);

app.use(express.static(__dirname + "/../client")); 

console.log(__dirname); //print out name of directory 
console.log(__dirname + "/../client"); 

app.post(EMPLOYEE_API, function(req,res){ //get values from front end 
    console.log(">>>" + JSON.stringify(req.body));
    res.status(200).json({});
});

app.get(EMPLOYEE_API, function(req,res){  //from service the get comes to here 
    console.log("Search >" + req.query.keyword); //this value must match query? key (keyword)
    var keyword = req.query.keyword;
    console.log(keyword);
    var whereClause = {limit: 10, where: {first_name: keyword}}; //this searches database (uses model/employees)
    Employees.findAll(whereClause).then(function(results){
        res.status(200).json(results); // return result from database -> service -> controller -> html
    }).catch((error)=>{
        console.log(errror);
    });    

});

app.use(function (req, res) {
    res.send("<h1>Page Not Found</h1>"); //json sending data to html
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

module.exports = app;