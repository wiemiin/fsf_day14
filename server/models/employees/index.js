
module.exports = function(connection, Sequelize){ 
    
        //employees match SQL table name
        //return Employees object is use within Javascript
        var Employees = connection.define('employees', { 
            emp_no: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            birth_date: {
                type: Sequelize.DATE,
                allowNull: false,
            },
            first_name: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            last_name: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            gender: {
                type: Sequelize.ENUM('M', 'F'),
                allowNull: false,
            }, 
            hire_date: {
                type: Sequelize.DATE,
                allowNull: false,
            }
        }, 
            
            {
                timestamps: false //Sequelize to not look for this column. If starting own project, turn it to true 
    
        });  
        return Employees; 
    }