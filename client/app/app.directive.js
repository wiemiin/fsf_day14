(function() {
    angular.module('EMSApp') 
    .directive("myCurrentTime", myCurrentTime);
    
    function myCurrentTime(dateFilter) { //dateFilter can be any name 
        return function(scope, element, attrs) { //fix names for these 3 parameters (attrs is attributes)
            console.log("---------------");
            var format;

            scope.$watch(attrs.myCurrentTime, function(value){ //listen to whatever changes to this DOM
                format = value;
                updateTime();
            });

            function updateTime(){
                var date = dateFilter(new Date(), format);
                element.text(date); //element of DOM
            }

            function updateLater() {
                setTimeout(function(){
                    updateTime();
                    updateLater();
                },1000)
            }
            updateLater();

        }
    }
})