(function() {
    angular
    .module("EMSApp")
    .service("EMSAppAPI", [
        '$http', 
        EMSAppAPI
    ]);

    function EMSAppAPI($http) { 
        var self = this;

        self.searchEmployees = function(keyword_value) {
            return $http.get("/api/employees?keyword=" + keyword_value); //because of $http it calls the get
        }

        self.submitPost = function(value) {
            var obj = {
                key: value
            };
            return $http.post("/api/employees", obj);
        }
    }

})();