(function () {
    "use strict";
    angular.module("EMSApp").controller("EmployeeController", EmployeeController);
    
    EmployeeController.$inject = ["EMSAppAPI"];

    function EmployeeController(EMSAppAPI) {
        var self = this;
        self.format = "hh:mm:ss";

        self.employees = []; 

        self.searchEmployees = searchEmployees;
        self.submitPost = submitPost;

        function searchEmployees() {
            console.log("Search employees... "); //get

            EMSAppAPI.searchEmployees(self.searchKeyword).then(function(results){ //this keyword calls function in service
                console.log(results);
                self.employees = results.data;
            }).catch(function (e) {
                console.log(e);
            }); 
        }

        function submitPost() {
            console.log("Post employees... "); //post

            EMSAppAPI.submitPost(self.searchKeyword).then(function(result){
                console.log(result);
            }).catch(function(e) {
                console.log(e);
            }); 
        }
        
    }

})();