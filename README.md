# SET UP #

* git init
* git remote add origin https://wiemiin@bitbucket.org/wiemiin/fsf_day14.git
* mkdir server directory > app.js
* mkdir client directory > app > app.module.js app.controller.js css > main.css
* create .gitignore (add node_modules and client/bower_components)
* npm init (create package.json, enter entry point server/app.js)
* npm install express body-parser sequelize - - save (root)
* npm install - - save mysql2
* bower init (create package.json, enter entry point server/app.js)
* create .bowerrc (directory: "client/bower_components")
* bower install bootstrap font-awesome angular - - save

# PUSH TO GIT #

* git add .
* git commit -m "update"
* git push origin master